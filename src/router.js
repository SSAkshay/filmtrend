import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Favorite from './views/Favorite.vue'

Vue.use(Router)

const router =  new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path:'/favorite',
      name:'favorite',
      component: Favorite
    }
  ]
})
export default router
